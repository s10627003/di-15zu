package sample;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;

public class Controller {
    public ImageView imageView;
    public Button str;
    public ImageView hero;
    public AnchorPane gameFrame;
    public ProgressBar playerHp;
    public ProgressBar bossHp;
    public Button vs;
    public ImageView boss;
    int flag=0;
    public void newWindow(ActionEvent event) throws IOException {
        //新視窗
        Parent root = FXMLLoader.load(getClass().getResource("start.fxml"));
        Stage stage2 = new Stage();
        stage2.setScene(new Scene(root, 500, 350));
        stage2.setResizable(false);
        stage2.setTitle("遊戲畫面");
        stage2.show();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        //新視窗

    }

    public void onStr(ActionEvent actionEvent) throws IOException {
            System.out.println("Start  Game");
            newWindow(actionEvent);
    }
    public int getHeroX(){  return (int)hero.getLayoutX(); }
    public int getHeroY(){  return (int)hero.getLayoutY(); }
    public int getBossX(){  return (int)boss.getLayoutX(); }
    public int getBossY(){  return (int)boss.getLayoutY(); }

    public void keyP (KeyEvent keyEvent) {
        int move=3;
        final int MAX_MOVE=450;
        final int MIN_MOVE=0;
        Task task= new Task<Void>(){
            @Override
            protected Void call() throws Exception {
                switch (keyEvent.getCode()){
                    case RIGHT:
                        Thread rThread=new Thread();
                        flag=1;
                        hero.setImage(new Image("/image/deep_000.gif"));
                        hero.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
                        if(hero.getLayoutX()<MAX_MOVE)
                        for (int i=0;i<=5;i++) {
                            rThread.sleep(10);
                            hero.setLayoutX(hero.getLayoutX() + move);
                            hero.setImage(new Image(getClass().getResource("/image/deep_0".concat(String.valueOf(i)).concat("0.gif")).toString()));
                        }
                        break;
                    case LEFT:
                        Thread lThread=new Thread();
                        flag=0;
                        hero.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
                        if(hero.getLayoutX()>=MIN_MOVE)
                        for (int i=0;i<5;i++) {
                            lThread.sleep(10);
                            hero.setLayoutX(hero.getLayoutX() - move);
                            hero.setImage(new Image(getClass().getResource("/image/deep_0".concat(String.valueOf(i)).concat("0.gif")).toString()));
                        }
                        break;
                    case UP:
                        Thread myThread=new Thread();
                        if(flag!=3) {
                        flag=3;
                            for (int i = 0; i < 25; i++) {
                                myThread.sleep(10);
                                hero.setLayoutY(hero.getLayoutY() - move);
                            }
                            for (int i = 0; i < 25; i++) {
                                myThread.sleep(10);
                                hero.setLayoutY(hero.getLayoutY() + move);
                            }
                        }
                        break;
                    case Z:
                        Thread atkThread=new Thread();
                        try {
                            atkThread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        for (int i=0;i<5;i++) {
                            atkThread.sleep(50);
                            hero.setImage(new Image("/image/deep_atk.gif"));
                        }
                        if (flag==0)
                            hero.setImage(new Image("/image/deep_001.gif"));
                        else
                            hero.setImage(new Image("/image/deep_000.gif"));

                        break;
                    case X:
                        Thread defThread=new Thread();
                        for (int i=0;i<5;i++) {
                            defThread.sleep(50);
                            hero.setImage(new Image("/image/def.gif"));
                        }
                        if (flag==0)
                            hero.setImage(new Image("/image/deep_001.gif"));
                        else
                            hero.setImage(new Image("/image/deep_000.gif"));
                        break;
                }
                return null;
            }

        };

        new  Thread(task).start();
    }


    public void keyIn(KeyEvent keyEvent) {
        keyP(keyEvent);
    }
}
